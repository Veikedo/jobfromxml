﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JobFromXml
{
    public static class RandomPeople
    {
        public static readonly Random Random = new Random();

        public static readonly Dictionary<int, string> LastName = new Dictionary<int, string>()
        {
            { 1, "Иванов" }, {2, "Смирнов" }, {3, "Кузнецов" }, {4, "Попов" }, {5, "Васильев" }, {6, "Петров" }, {7, "Соколов" }, {8, "Михайлов" }, {9, "Новиков" }, {10, "Федоров" }
        };

        public static readonly Dictionary<int, string> FirstName = new Dictionary<int, string>()
        {
            { 1, "Александр" }, {2, "Иван" }, {3, "Пётр" }, {4, "Руслан" }, {5, "Сергей" }, {6, "Тимофей" }, {7, "Михаил" }, {8, "Николай" },  {9, "Евгений" }, {10, "Виталий" }
        };

        public static readonly Dictionary<int, string> Speciality = new Dictionary<int, string>()
        {
            { 1, "Артист" }, {2, "Ветеринар" }, {3, "Врач" }, {4, "Геолог" }, {5, "Кинолог" }, {6, "Переводчик" }, {7, "Репортер" }, {8, "Учитель" },  {9, "Экономист" }, {10, "Юрист" }
        };

        public static readonly Dictionary<int, string> Minor = new Dictionary<int, string>()
        {
            { 1, "Студент" }, {2, "Стажер" }, {3, "Абитуриент" }, {4, "Волонтер" }, {5, "Школьник" }
        };    

        public static Guid GetGuid() => Guid.NewGuid();

        public static string GetRandomLastName() => LastName[Random.Next(1, LastName.Count + 1)];
        public static string GetRandomFirstName() => FirstName[Random.Next(1, FirstName.Count + 1)];
        public static string GetRandomSpeciality() => Speciality[Random.Next(1, Speciality.Count + 1)];
        public static string GetRandomMinor() => Minor[Random.Next(1, Minor.Count)];
        public static int GetRandomAge() => Random.Next(14, 45);
        public static string GetRandomStatus(int Age)
        {
            return Age < 17 ? Minor[5] : Age >= 17 && Age <= 22 ? RandomPeople.GetRandomMinor() : RandomPeople.GetRandomSpeciality();    
        }

        public static string GetLastName()
        {
            return RandomPeople.GetRandomLastName();
        }

        public static string GetFirstName()
        {
            return RandomPeople.GetRandomFirstName();
        }

        public static string GetSpeciality()
        {
            return RandomPeople.GetRandomSpeciality();
        }

        public static int GetAge()
        {
            return RandomPeople.GetRandomAge();
        }

    }
}

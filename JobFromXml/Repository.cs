﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JobFromXml
{
    class Repository : IRepository<Account>
    {
        private readonly List<Account> _collection;
        private const string repositoryFileName = "repository.json";

        public Repository()
        {
            _collection = new List<Account>();

            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;
            using var sw = new StreamReader(repositoryFileName);
            _collection = JsonConvert.DeserializeObject<List<Account>>(sw.ReadToEnd());
        }

        public void Add(Account item)
        {
            _collection.Add(item);

            var serializer = new JsonSerializer
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            using var sw = new StreamWriter(repositoryFileName, false);
            using var writer = new JsonTextWriter(sw);
            serializer.Serialize(writer, _collection);
        }

        public IEnumerable<Account> GetAll()
        {
            foreach (var item in _collection)
            {
                yield return item;
            }
        }
        public Account GetOne(Func<Account, bool> predicate) => _collection.FirstOrDefault(predicate);
    }
}

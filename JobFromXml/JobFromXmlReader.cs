﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JobFromXml
{
    public class JobFromXmlReader : IEnumerable<Person>, IDisposable, IAlgorithm
    {
        private readonly Stream _stream;
        private List<Person> _people = new List<Person>();

        #region Десериализация Xml
        public JobFromXmlReader(Stream stream)
        {
            _stream = stream ?? throw new ArgumentNullException(nameof(stream));
            using var reader = new StreamReader(_stream);
            var xml = reader.ReadToEnd();

            if (string.IsNullOrEmpty(xml))
            {
                throw new ArgumentNullException();
            }

            IExtendedXmlSerializer serializer = new ConfigurationContainer().ConfigureType<Person>().Create();
            _people = serializer.Deserialize<List<Person>>(xml);

        }

        public IEnumerator<Person> GetEnumerator()
        {
            return ((IEnumerable<Person>)_people).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            _stream.Dispose();
        }

        #endregion

        #region Сортировка
        public void SortByAge()
        {
            var sorterPerson = from p in _people
                               orderby p.Age
                               select p;
            foreach (Person p in sorterPerson)
                Console.WriteLine(p);
        }

        public void SortByLastName()
        {
            var sorterPerson = from p in _people
                               orderby p.LastName
                               select p;
            foreach (Person p in sorterPerson)
                Console.WriteLine(p);
        }

        public void SortByFirstName()
        {
            var sorterPerson = from p in _people
                               orderby p.FirstName
                               select p;
            foreach (Person p in sorterPerson)
                Console.WriteLine(p);
        }

        public void SortBySpeciality()
        {
            var sorterPerson = from p in _people
                               orderby p.Speciality
                               select p;
            foreach (Person p in sorterPerson)
                Console.WriteLine(p);
        }

        public void SortByBirthDate()
        {
            var sorterPerson = from p in _people
                               orderby p.BirthDate
                               select p;
            foreach (Person p in sorterPerson)
                Console.WriteLine(p);
        }
        #endregion

    }
}

﻿using System.Collections.Generic;

namespace JobFromXml
{
    public interface IAlgorithm
    {
        void SortByAge();
        void SortByLastName();
        void SortByFirstName();
        void SortBySpeciality();
        void SortByBirthDate();
    }
}

﻿using Newtonsoft.Json;
using System;

namespace JobFromXml
{
    public class Account : Person
    {
        public Account(string LastName, string FirstName, int Age) : base(LastName, FirstName, Age)
        {
            BirthDate = DateTime.Today.AddYears(-Age);
        }

        public override string ToString()
        {            
            return $"{LastName}    \t{FirstName}    \t{BirthDate.ToShortDateString()} \tВозраст: {Age}";
        }
    }
}
